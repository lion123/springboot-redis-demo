package com.example.redis;

import com.alibaba.fastjson.JSONObject;
import com.example.redis.util.RedisLock;
import com.example.redis.util.RedisLockUtil;
import com.example.redis.util.RedisUtils;
import com.example.redis.util.SpringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootRedisDemoApplication.class)
@ActiveProfiles(profiles = "dev")
public class RedisTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisTests.class);

	@Autowired
    private Jedis jedis;
    @Autowired
	private RedisUtils redisUtil;
    @Autowired
	private RedisLock redisLock;

    @Before
    public void init(){
        if (jedis == null)
            jedis = SpringUtils.getBean("jedis");
    }

	@Test
	public void testLock() throws InterruptedException {
		Lock test = redisLock.lock("test");
		TimeUnit.SECONDS.sleep(5);
		redisLock.unlock(test);
	}


	@Test
	public void redisLockUtilTest() throws InterruptedException {
        RedisLockUtil redisLockUtil = new RedisLockUtil("testLock1", "1");
        redisLockUtil.lock();
		TimeUnit.SECONDS.sleep(10);
        redisLockUtil.unlock();
	}


	@Test
	public void testRedisUtil() {
        String str = null;

        jedis.set("name", "test centent value 1 ");

        str = jedis.get("name");
        System.out.println("1、>>>>>>str: "+str);

        Optional strOp = redisUtil.get("name");
        System.out.println("2、>>>>>>str: "+strOp.get());

		redisUtil.setStr("name", "test centent  value 2 ");
		str = redisUtil.getStr("name");
		System.out.println("3、>>>>>>str: "+str);
	}

	@Test
	public void test1() {
		redisUtil.setStr("name", "test1 redis value11111111111111111");
		String str =  redisUtil.getStr("name");
		System.out.println("str: ============================================================= ");
		System.out.println(">>>>>>str: "+str);
	}

	@Test
	public void test2() {
		String enumerationName="GE_MPL_BRAND_TYPE";

		fixedDataInit(enumerationName);

		enumerationName="GE_MPL_BRAND_TYPE";
		List<Map<Object, Object>> getFixedDataList = getFixedDataList( enumerationName);

		String field="OWN";
		Map<Object,Object> getFixedDataMap = getFixedDataMap(enumerationName,field);

		field="OWN";
		Object getFixedDataValue = getFixedDataValue(enumerationName, field);

		field="自有";
		String getFixedDataKey = getFixedDataKey(enumerationName, field);
	}

	public void fixedDataInit(String enumerationName) {
		try {
			String CacheKey_FIXED_DATA = "CACHE_RDM_FIXED_DATA_65";
			String fieldname = CacheKey_FIXED_DATA + enumerationName;

			List<Map<String, Object>> fixedDataList = setFixedDataList();
			if (!CollectionUtils.isEmpty(fixedDataList)){
				String key = null;
				String val = null;
				String val_en = null;
				String val_ja = null;
				for (Map<String,Object> fixedData : fixedDataList){
					key = String.valueOf(fixedData.get("KEY"));
					val = String.valueOf(fixedData.get("VALUE"));
					val_en = String.valueOf(fixedData.get("VALUE_EN_US"));
					val_ja = String.valueOf(fixedData.get("VALUE_JA_JP"));

                    redisUtil.hset("ja_jp" + fieldname,key,val_ja);
                    redisUtil.hset("en_us" + fieldname,key,val_en);
                    redisUtil.hset("zh_cn" + fieldname,key,val);
				}
				redisUtil.setStr(fieldname,JSONObject.toJSONString(fixedDataList));
			}
		}catch (Exception e){
			LOGGER.error("FixedDataInit异常", e);
		}
	}


	/**
	 * 获取指定plm的枚举值
	 * @param enumerationName 枚举值内部名字
	 * @param field 枚举值的key
	 * @return 根据language返回value*列的内容
	 */
	public Object getFixedDataValue(String enumerationName,String field) {
		String CacheKey_FIXED_DATA = "CACHE_RDM_FIXED_DATA_65";
		String fieldname = CacheKey_FIXED_DATA + enumerationName;
		String language = getRedisLanguagePreKeyForFixedData();

		boolean hasKey = redisUtil.hHasKey(language + fieldname, field);
		if (!hasKey){
			fixedDataInit(enumerationName);
		}
		Object result = redisUtil.hget(language + fieldname, field);
		return result;
	}

	/**
	 * 获取指定plm的枚举key
	 * @param enumerationName 枚举值内部名字
	 * @param field 枚举值的value
	 * @retu 返回key列的内容
	 */
	public String getFixedDataKey(String enumerationName,String field) {
		Map<Object,Object> map = getFixedDataMap(enumerationName,field);
		if (null != map && map.size() > 0){
			Object value = null;
			Object resultKey = map.get("KEY");
			for (Map.Entry<Object,Object> entry :map.entrySet()){
				value = entry.getValue();
				if (!StringUtils.isEmpty(value) && field.equalsIgnoreCase(String.valueOf(value))){
					return StringUtils.isEmpty(resultKey)? null : String.valueOf(resultKey);
				}
			}
		}
		return null;
	}

	/**
	 * 获取指定plm的枚举值map
	 * @param enumerationName 枚举值内部名字
	 * @param field 枚举值的key
	 * @return
	 */
	public Map<Object,Object> getFixedDataMap (String enumerationName,String field) {
		List<Map<Object, Object>> getFixedDataList = getFixedDataList( enumerationName);
		if (!CollectionUtils.isEmpty(getFixedDataList)){
			for (Map<Object, Object> map : getFixedDataList){
				for (Map.Entry entry:map.entrySet()){
					if (!StringUtils.isEmpty(field) && !StringUtils.isEmpty(entry.getValue())
							&& field.equalsIgnoreCase(String.valueOf(entry.getValue()))){
						return map;
					}
				}
			}
		}
		return null;
	}

	public List<Map<Object, Object>> getFixedDataList(String enumerationName){
		String CacheKey_FIXED_DATA = "CACHE_RDM_FIXED_DATA_65";
		String fieldname = CacheKey_FIXED_DATA + enumerationName;

		boolean hasKey = redisUtil.hasKey(fieldname);
		if (!hasKey){
			fixedDataInit(enumerationName);
		}
		List<Map<Object, Object>> result = null;
		String str = redisUtil.getStr(fieldname);
		if (!StringUtils.isEmpty(str)){
			result = JSONObject.parseObject(str,List.class);
		}
		return result;
	}

   private List<Map<String, Object>>  setFixedDataList(){
	   //fi.key,fi.value,fi.value_en_us,fi.value_ja_jp
	   List<Map<String, Object>> fixedDataList = new ArrayList<>();

	   Map<String, Object> fixedData = new HashMap<>();
	   fixedData.put("KEY", "OWN");
	   fixedData.put("VALUE", "自有");
	   fixedData.put("VALUE_EN_US", "Own");
	   fixedData.put("VALUE_JA_JP", "自社");
	   //fixedDataList.add(fixedData);

	   Map<String, Object> fixedData2 = new HashMap<>();
	   fixedData2.put("KEY", "LEASE");
	   fixedData2.put("VALUE", "租赁");
	   fixedData2.put("VALUE_EN_US", "Lease");
	   fixedData2.put("VALUE_JA_JP", "リース");
	   //fixedDataList.add(fixedData2);

	   Map<String, Object> fixedData3 = new HashMap<>();
	   fixedData3.put("KEY", "CUSTOMER");
	   fixedData3.put("VALUE", "客户");
	   fixedData3.put("VALUE_EN_US", "CUSTOMER");
	   fixedData3.put("VALUE_JA_JP", "顧客");
	   //fixedDataList.add(fixedData3);

	   return fixedDataList;
   }

   private String getRedisLanguagePreKeyForFixedData(){
	   String language = "ZH-CN";
	   if (language.contains("CN") || language.contains("cn")){
		   language = "zh_cn";
	   } else if (language.contains("US") || language.contains("us")){
		   language = "en_us";
	   } else if (language.contains("JP") || language.contains("jp")){
		   language = "ja_jp";
	   }
	   return language;
   }

}
