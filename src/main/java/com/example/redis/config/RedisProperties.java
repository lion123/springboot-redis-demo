package com.example.redis.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

/**
 * @Auther: liunanhua
 * @Date: 2019/3/6 14:43
 * @Description:
 */
@Component
@ConfigurationProperties(prefix = "spring.redis")
public class RedisProperties {

    private int database = 0;

    private String url;

    private String host = "localhost";

    private String password;

    private int port = 6379;

    private boolean ssl;

    private Duration timeout;

    private Sentinel sentinel;

    private Cluster cluster;

    private final Jedis jedis = new Jedis();

    private final Lettuce lettuce = new Lettuce();

    /**
     * Pool properties.（连接池的配置信息）
     */
    @Component
    public class Pool {

        @Value(value = "${spring.redis.jedis.pool.max-active:8}")
        private int maxActive;

        @Value(value = "${spring.redis.jedis.pool.max-wait:-1}")
        private int maxWait;

        @Value(value = "${spring.redis.jedis.pool.max-idle:100}")
        private int maxIdle;

        @Value(value = "${spring.redis.jedis.pool.min-idle:1}")
        private int minIdle;

        //private Duration maxWait = Duration.ofMillis(-1);

        public int getMaxIdle() {
            return maxIdle;
        }

        public void setMaxIdle(int maxIdle) {
            this.maxIdle = maxIdle;
        }

        public int getMinIdle() {
            return minIdle;
        }

        public void setMinIdle(int minIdle) {
            this.minIdle = minIdle;
        }

        public int getMaxActive() {
            return maxActive;
        }

        public void setMaxActive(int maxActive) {
            this.maxActive = maxActive;
        }

        public int getMaxWait() {
            return maxWait;
        }

        public void setMaxWait(int maxWait) {
            this.maxWait = maxWait;
        }

        /*public Duration getMaxWait() {
            return maxWait;
        }

        public void setMaxWait(Duration maxWait) {
            this.maxWait = maxWait;
        }*/
    }

    /**
     * Cluster properties.（集群配置信息）
     */
    @Component
    public class Cluster {
        //@Value(value = "${spring.redis.cluster.nodes:127.0.0.1:6080}")
        private List<String> nodes;
        //@Value(value = "${spring.redis.cluster.max-redirects:10}")
        private Integer maxRedirects;

        public List<String> getNodes() {
            return nodes;
        }

        public void setNodes(List<String> nodes) {
            this.nodes = nodes;
        }

        public Integer getMaxRedirects() {
            return maxRedirects;
        }

        public void setMaxRedirects(Integer maxRedirects) {
            this.maxRedirects = maxRedirects;
        }
    }

    /**
     * Redis sentinel properties.（哨兵属性信息）
     */
    //@Component
    //@ConfigurationProperties(prefix = "spring.redis.sentinel")
    public class Sentinel {

        private String master;

        private String nodes;

        private List<String> nodesList;


        public String getMaster() {
            return master;
        }

        public void setMaster(String master) {
            this.master = master;
        }

        public String getNodes() {
            return nodes;
        }

        public void setNodes(String nodes) {
            this.nodes = nodes;
        }

        public List<String> getNodesList() {
            this.nodesList = Arrays.asList(this.nodes.split(","));
            return nodesList;
        }

        public void setNodesList(List<String> nodesList) {
            this.nodesList = nodesList;
        }
    }

    /**
     * Jedis client properties.（redis的客户端jedis）
     */
    public class Jedis {

        /**
         * Jedis pool configuration.
         */
        private Pool pool;

        public Pool getPool() {
            return pool;
        }

        public void setPool(Pool pool) {
            this.pool = pool;
        }
    }

    /**
     * Lettuce client properties.
     */
    public class Lettuce {

        /**
         * Shutdown timeout.
         */
        private Duration shutdownTimeout = Duration.ofMillis(100);

        /**
         * Lettuce pool configuration.
         */
        private Pool pool;

        public Duration getShutdownTimeout() {
            return shutdownTimeout;
        }

        public void setShutdownTimeout(Duration shutdownTimeout) {
            this.shutdownTimeout = shutdownTimeout;
        }

        public Pool getPool() {
            return pool;
        }

        public void setPool(Pool pool) {
            this.pool = pool;
        }
    }

    public int getDatabase() {
        return database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public Duration getTimeout() {
        return timeout;
    }

    public void setTimeout(Duration timeout) {
        this.timeout = timeout;
    }

    public Sentinel getSentinel() {
        return sentinel;
    }

    public void setSentinel(Sentinel sentinel) {
        this.sentinel = sentinel;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    public Jedis getJedis() {
        return jedis;
    }

    public Lettuce getLettuce() {
        return lettuce;
    }
}
