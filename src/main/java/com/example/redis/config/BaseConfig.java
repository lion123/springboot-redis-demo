package com.example.redis.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by liunanhua on 2018/3/19.
 */
@Slf4j
@Configuration
public class BaseConfig {

    @Value("${spring.application.name}")
    private String appName;
    @Value("${eureka.instance.hostname:localhost}")
    private String hostname;
    @Value("${server.port:8080}")
    private int port;


    @Bean(name = "hostName")
    public String getHostName() {
        log.info(">>>>>> 访问 http://{}:{} 可以查看{}服务信息", hostname, port, appName);
        try {
            if (StringUtils.isNotBlank(appName)) {
                appName = InetAddress.getLocalHost().getHostName();
            }
        } catch (UnknownHostException e) {
            log.error(">>>>>> UnknownHostException", e);
        }
        return appName;
    }


}
